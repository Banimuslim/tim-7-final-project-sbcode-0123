@extends('layouts.master')
@section('title')
Halaman Update Profile
@endsection

@section('content')
<form action="/profile/{{$detailProfiles->id}}" method="POST">
    @csrf
    @method('put')
    
    <div class="form-group" >
        <label>Nama User</label>
        <input type="text" class="form-control" value="{{$detailProfiles->user->name}}" disabled>
    </div>

    <div class="form-group" >
        <label>Email</label>
        <input type="text" class="form-control" value="{{$detailProfiles->user->email}}" disabled>
    </div>

    <div class="form-group" >
      <label>Umur</label>
      <input type="number" class="form-control" value="{{$detailProfiles->umur}}" name="umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$detailProfiles->bio}}</textarea>
      </div>
      @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label>Alamat</label>
      <input type="text" class="form-control" value="{{$detailProfiles->alamat}}" name="alamat">
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

