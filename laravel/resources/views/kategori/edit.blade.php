@extends('layouts.master')

@section('title')
	Form Edit Kategori
@endsection

@section('content')

	<form action="/kategori/{{$kategori->id}}" method="post">
		@csrf
		@method('put')
		<div class="form-group">	
			<label>Nama kategori:</label>
			<input type="text" class="form-control" name="nama" value="{{$kategori->nama}}">
		</div>
		@error('nama')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror

		<input type="submit" name="submit" value="Edit Data!">
	</form>
@endsection
