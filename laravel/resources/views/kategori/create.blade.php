@extends('layouts.master')

@section('title')
	Data Kategori
@endsection

@section('content')

	<form action="/kategori" method="post">
		@csrf
		<div class="form-group">	
			<label>Nama Kategori Baru:</label>
			<input type="text" class="form-control" name="nama">
		</div>
		@error('nama')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		
		<input type="submit" name="submit">
	</form>
@endsection
