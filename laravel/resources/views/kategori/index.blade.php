@extends('layouts.master')

@section('title')
	Data Kategori
@endsection
@push('scripts')
<script src="{{asset('sbadmin2/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('sbadmin2/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $(".table").DataTable();
  });
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css"/>
@endpush
@section('content')

  	@auth
	<a href="/kategori/create" class="btn btn-primary btn-sm my-2">Tambah Kategori</a>
  	@endauth

	<table class="table">
		<thead>
		<tr>
			<th>No.</th>
			<th>Nama Kategori</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody>
		@forelse($kategori as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->nama}}</td>
				<td>
					<form action="/kategori/{{$value->id}}" method="post">
						@csrf
						@method('delete')
						<a href="/kategori/{{$value->id}}" class="btn btn-info btn-sm">Lihat Buku</a>
						@auth
						<a href="/kategori/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
						<input type="submit" value="delete" class="btn btn-danger btn-sm">
						@endauth
					</form>
				</td>
			</tr>
		@empty
			<tr><td>tidak ada data kategori</td></tr>
		@endforelse
		</tbody>
	</table>
@endsection
