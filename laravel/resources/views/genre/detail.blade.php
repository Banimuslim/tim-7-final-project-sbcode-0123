@extends('layouts.master')

@section('title')
	Buku dengan genre: {{$genre->nama}}
@endsection

@section('content')

<div class="row">
	@forelse($genre->ambilGenre as $item)
		<div class ="col-4"> 
		<div class="card" style="width: 20rem;">
                <img src="{{asset('/image/'.$item->cover)}}" width="300" height="400" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">{{$item->nama}}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">{{$item->penulis}}</h6>
                  <h6 class="text-primary mb-2">{{$item->penerbit}}</h6>
                  <p class="card-text">{{ Str::limit($item->sinopsis, 40) }}</p>
                  <form action="/buku/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/buku/{{$item->id}}" class="card-link">Detail</a>
                    <a href="/buku/{{$item->id}}/edit" class="card-link">Edit</a>
                    <input type="submit" class="btn btn-danger ml-3" value="Delete">
                
                  </form>
                </div>
            </div>

		</div>
	@empty
		<p>tidak ada data</p>
	@endforelse
</div>
@endsection
