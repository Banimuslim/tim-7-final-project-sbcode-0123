@extends('layouts.master')

@section('title')
	Form Edit Genre
@endsection

@section('content')

	<form action="/genre/{{$genre->id}}" method="post">
		@csrf
		@method('put')
		<div class="form-group">	
			<label>Nama genre:</label>
			<input type="text" class="form-control" name="nama" value="{{$genre->nama}}">
		</div>
		@error('nama')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror

		<input type="submit" name="submit" value="Edit Genre!">
	</form>
@endsection
