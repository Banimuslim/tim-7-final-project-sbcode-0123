@extends('layouts.master')

@section('title')
	Data Genre
@endsection

@section('content')

	<form action="/genre" method="post">
		@csrf
		<div class="form-group">	
			<label>Nama Genre Baru:</label>
			<input type="text" class="form-control" name="nama">
		</div>
		@error('nama')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		
		<input type="submit" name="submit">
	</form>
@endsection
