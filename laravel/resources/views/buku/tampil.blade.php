@extends('layouts.master')
@section('title')
Halaman Isi Buku
@endsection


@section('content')
@auth
<div class="container ml-1 mb-4">
    <div class="row">
        <div class="col-sm-2">
            <div class="card-body pb-0 px-0 px-md-4">
              <img
                src="{{asset('gambar/man.png')}}"
                height="140"
                alt="View Badge User"
              />
            </div>
          </div>
        <div class="col-sm-6">
            <div class="card-body">
              <h5 class="card-title text-primary">Hallo {{ Auth::user()->name }}</h5>
              <p class="mb-4">
                Selamat Datang Di Library Sistem. Disini tersedia banyak referensi buku berdasarkan genre dan kategori yang kamu suka, dan kamu bisa memberi ulasan pada setiap buku juga lho. Selamat Membaca
              </p>
            </div>
        </div>

    </div>
</div>
@endauth
<div class="container-fluid mt-2">
    <div class="row">
        @forelse ($buku as $item)
        <div class="col-3">
            <div class="card" style="width: 20rem;">
                <img src="{{asset('/image/'.$item->cover)}}" width="300" height="400" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">{{$item->nama}}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">{{$item->penulis}}</h6>
                  <h6 class="text-primary mb-2">{{$item->penerbit}}</h6>
                  <p class="card-text">{{ Str::limit($item->sinopsis, 40) }}</p>
                  <form action="/buku/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/buku/{{$item->id}}" class="card-link">Detail</a>
                    @auth
                    <a href="/buku/{{$item->id}}/edit" class="card-link">Edit</a>
                    <input type="submit" class="btn btn-danger ml-3" value="Delete">
                    @endauth
                  </form>
                </div>
            </div>
        </div>   
        @empty
            <h3>Tidak Ada Buku</h3>
        @endforelse
    </div>
</div>

@endsection
