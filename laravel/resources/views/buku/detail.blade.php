@extends('layouts.master')
@section('title')
Halaman Buku
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/cfbhbgfguou2hj07pkxlk9lur44snnbx40k90rzxoq7gj8ss/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
    toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    mergetags_list: [
      { value: 'First.Name', title: 'First Name' },
      { value: 'Email', title: 'Email' },
    ]
  });
</script>
@endpush

@section('content')
<div class="container-sm ml-1">
    <div class="row">
      <div class="col-5">
        <img src="{{asset('/image/'.$buku->cover)}}" width="400" height="500">
      </div>
      <div class="col-7">
        <h5 class="card-title">{{$buku->nama}}</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{$buku->penulis}}</h6>
        <h6 class="text-primary mb-2">{{$buku->penerbit}}</h6>
        <p class="card-text">{{$buku->sinopsis}}</p> 
        <div class="badge badge-primary text-wrap" style="width: 6rem;">
           {{$buku->tangkapKategori->nama}}
        </div>
        <div class="badge badge-primary text-wrap" style="width: 6rem;">
           {{$buku->tangkapGenre->nama}}
        </div>
        <br>
      </div>
    </div>
</div
<hr>
<hr>
<hr>
  <h4>List Ulasan</h4>

  @forelse ($buku->ulas as $item)
  <div class="card my-2">
    <div class="card-header">
      {{$item->user->name}}
    </div>
    <div class="card-body">
      <p class="card-text">{!!$item->ulasan!!}</p>
    </div>
  </div>
  @empty
  <h6>Belum Pernah di Ulas, Jadilah Orang Petama yang Mengulas</h6>
  @endforelse

<hr>
@auth
<form action="/ulasan/{{$buku -> id}}" method="POST">
  @csrf
  <textarea name="ulasan" class="form-control" placeholder="Tambah Ulasan" id="" cols="30" rows="10"></textarea>
  @error('ulasan')
        <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <input type="submit" value="tambah" class="btn btn-primary mt-3">
</form>
@endauth
<a href="/buku"><button class="btn btn-info mt-3">Kembali</button></a>
@endsection

