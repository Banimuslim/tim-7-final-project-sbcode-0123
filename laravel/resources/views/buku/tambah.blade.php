@extends('layouts.master')
@section('title')
Halaman Tambah Buku
@endsection

@section('content')
<form method="POST" enctype="multipart/form-data" action="/buku">
    @csrf
    <div class="form-group">
      <label>Nama Buku</label>
      <input type="text" class="form-control" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Penulis</label>
      <input type="text" class="form-control" name="penulis">
    </div>
    @error('penulis')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Penerbit</label>
      <input type="text" class="form-control" name="penerbit">
    </div>
    @error('penerbit')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Sinopsis</label>
      <textarea name="sinopsis" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    @error('sinopsis')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>cover</label>
        <input type="file" class="form-control" name="cover">
      </div>
      @error('cover')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label>Kategori Buku</label>
      <select name="kategori_id" id="" class="form-control">
        <option value="">--Pilih Kategori--</option>
        @forelse ($kategori as $item)
            <option value="{{$item ->id}}">{{$item -> nama}}</option>
        @empty
            
        @endforelse
      </select>
    </div>
    @error('kategori')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Genre</label>
      <select name="genre_id" id="" class="form-control">
        <option value="">--Pilih Genre--</option>
        @forelse ($genre as $item)
            <option value="{{$item ->id}}">{{$item -> nama}}</option>
        @empty
            
        @endforelse
      </select>
    </div>
    @error('genre')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

