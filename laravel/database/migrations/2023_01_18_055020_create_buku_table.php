<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('penulis');
            $table->string('penerbit');
            $table->text('sinopsis');
            $table->string('cover');
            $table->unsignedBigInteger('genre_id');
            $table->unsignedBigInteger('kategori_id');
            $table->foreign('genre_id')->references('id')->on('genre');
            $table->foreign('kategori_id')->references('id')->on('kategori');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
