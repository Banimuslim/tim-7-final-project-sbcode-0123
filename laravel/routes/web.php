<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\KatController;
use App\Http\Controllers\UlasanController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\GenController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/', [MainController::class, 'home']);





Route::group(['middleware' => ['auth']], function () {
Route::resource('profile',ProfileController::class)->only(['index', 'update']);

// Route::get('/kategori', [KategoriController::class, 'tampilKategori']);
// Route::post('/kategori', [KategoriController::class, 'store']);
// Route::get('/genre', [GenreController::class, 'tampilGenre']);
// Route::post('/genre', [GenreController::class, 'store']);
// Route::get('/kategori/{kategori_id}', [KategoriController::class, 'show']);
// Route::get('/genre/{genre_id}', [GenreController::class, 'show']);

// Route::get('/kategori/create', [KategoriController::class, 'create']);
// Route::get('/kategori/{kategori_id}/edit', [KategoriController::class, 'edit']);
// Route::put('/kategori/{kategori_id}', [KategoriController::class, 'update']);
// Route::delete('/kategori/{kategori_id}', [KategoriController::class, 'destroy']);

// //CRUD Genre


// Route::get('/genre/create', [GenreController::class, 'create']);

// Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit']);
// Route::put('/genre/{genre_id}', [GenreController::class, 'update']);
// Route::delete('/genre/{genre_id}', [GenreController::class, 'destroy']);

//CRUD BUKU
Route::post('/ulasan/{buku_id}', [UlasanController::class, 'store']);

});

Route::resource('buku', BukuController::class);
Route::resource('kategori', KatController::class);
Route::resource('genre', GenController::class);


Auth::routes();


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
