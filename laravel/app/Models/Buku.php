<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    use HasFactory;

    protected $table = 'buku';

    protected $fillable = ['nama','penulis','penerbit','sinopsis','cover','genre_id','kategori_id'];

    public function tangkapKategori(){
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }
    
    public function tangkapGenre(){
        return $this->belongsTo(Genre::class, 'genre_id');
    }

    public function ulas(){
        return $this->hasMany(Ulasan::class, 'buku_id');
    }
}
