<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ulasan extends Model
{
    use HasFactory;

    protected $table = 'ulasan';
    protected $fillable = ['ulasan', 'buku_id', 'users_id'];

    public function user(){
        return $this->belongsTo(User::class, 'users_id');
    }

}
