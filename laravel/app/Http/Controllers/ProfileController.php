<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use GuzzleHttp\Handler\Proxy;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index(){
        $idUser = Auth::id();

        $detailProfiles = Profile::where('users_id', $idUser)->first();

        return view('perpus.index', ['detailProfiles' => $detailProfiles]);
    
    }
    public function update(Request $request, $id){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required'
        ]);
            $profile = Profile::find($id);

            $profile->umur = $request->umur;
            $profile->bio = $request->bio;
            $profile->alamat = $request->alamat;


            $profile->save();

            return redirect('/profile')->withSuccess('Profil Diupdate!!');
    }
    
}
