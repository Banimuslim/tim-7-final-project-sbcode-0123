<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ulasan;
use Illuminate\Support\Facades\Auth;

class UlasanController extends Controller
{
    public function store(Request $request, $id){
        $request->validate([
            'ulasan' => 'required'
        ]);

        $idUsers = Auth::id();

        $ulasan = new Ulasan();
        $ulasan -> ulasan = $request['ulasan'];
        $ulasan -> buku_id = $id;
        $ulasan -> users_id = $idUsers;
        $ulasan->save();

        return redirect('/buku/'. $id);
    }
}
