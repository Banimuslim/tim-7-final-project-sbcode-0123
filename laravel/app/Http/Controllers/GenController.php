<?php

namespace App\Http\Controllers;
use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenController extends Controller
{

    public function __construct()
     {
 
         $this->middleware('auth')->except('index', 'show');
 
     } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genre = DB::table('genre')->get();
        return view('genre.index', ['genre' => $genre] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('genre.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        // memasukan data request ke table kategori di database
        DB::table('genre')->insert([
            'nama' => $request['nama']
        ]);
        
        // arahkan ke halaman /kategori
        return redirect ('/genre')->withSuccess('Genre Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genre = Genre::find($id);
        $buku = DB::table('buku')->where('genre_id', $id)->get();

        return view('genre.detail', ['genre' => $genre, 'buku' => $buku]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->find($id);

        return view('genre.edit', ['genre' => $genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        DB::table('genre')
            ->where('id',$id)
            ->update([
                'nama' => $request['nama']
                ]
            );
        return redirect('/genre')->withSuccess('Genre Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('genre')->where('id',$id)->delete();

        return redirect('/genre')->withSuccess('Category Berhasi Dihapus');
    }
}
