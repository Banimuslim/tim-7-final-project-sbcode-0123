<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Buku;

class MainController extends Controller
{
    public function home()
    {
        $buku = Buku::all();
        return view('perpus.home', ['buku' => $buku]);
    }
}
