<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class KatController extends Controller
{

    public function __construct()
     {
 
         $this->middleware('auth')->except('index', 'show');
 
     } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = DB::table('kategori')->get();
        return view('kategori.index', ['kategori' => $kategori] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        // memasukan data request ke table kategori di database
        DB::table('kategori')->insert([
            'nama' => $request['nama']
        ]);
        
        // arahkan ke halaman /kategori
        return redirect ('/kategori')->withSuccess('Kategori Baru Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = DB::table('kategori')->find($id);
        $buku = DB::table('buku')->where('kategori_id', $id)->get();

        return view('kategori.detail', ['kategori' => $kategori, 'buku' => $buku]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->find($id);

        return view('kategori.edit', ['kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        DB::table('kategori')
            ->where('id',$id)
            ->update([
                'nama' => $request['nama']
                ]
            );
        return redirect('/kategori')->withSuccess('Kategori Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('kategori')->where('id',$id)->delete();

        return redirect('/kategori')->withSuccess('Kategori Berhasil Dihapus');
    }
}
