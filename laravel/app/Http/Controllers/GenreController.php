<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Genre;

class GenreController extends Controller
{

    public function tampilGenre(){
        $genre = DB::table('genre')->get();
        return view('genre.index', ['genre' => $genre] );
    }
    public function create(){
        return view('genre.create');
    }
    
    public function store(request $request){

        // validasi data
        $request->validate([
            'nama' => 'required'
        ]);

        // memasukan data request ke table kategori di database
        DB::table('genre')->insert([
            'nama' => $request['nama']
        ]);
        
        // arahkan ke halaman /kategori
        return redirect ('/genre')->withSuccess('Genre Berhasil Ditambahkan');
    }

    public function show($id){
        $genre = Genre::find($id);
        $buku = DB::table('buku')->where('genre_id', $id)->get();

        return view('genre.detail', ['genre' => $genre, 'buku' => $buku]);
    }

    public function edit($id){
        $genre = DB::table('genre')->find($id);

        return view('genre.edit', ['genre' => $genre]);
    }

    public function update($id, request $request){
        // validasi data
        $request->validate([
            'nama' => 'required'
        ]);

        DB::table('genre')
            ->where('id',$id)
            ->update([
                'nama' => $request['nama']
                ]
            );
        return redirect('/genre')->withSuccess('Genre Berhasil Diupdate');
    }

    public function destroy($id){
        DB::table('genre')->where('id',$id)->delete();

        return redirect('/genre')->withSuccess('Category Berhasi Dihapus');
    }
}
