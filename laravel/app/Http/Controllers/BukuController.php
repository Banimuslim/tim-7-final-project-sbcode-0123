<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;
use App\Models\Kategori;
use App\Models\Buku;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\File;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
 
         $this->middleware('auth')->except('index', 'show');
 
     } 

    public function index()
    {
        $buku = Buku::all();
        return view('buku.tampil', ['buku' => $buku]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        $genre = Genre::all();
        return view('buku.tambah', ['kategori' => $kategori, 'genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255',
            'penulis' => 'required',
            'penerbit' => 'required',
            'sinopsis' => 'required',
            'kategori_id' => 'required',
            'genre_id' => 'required',
            'cover' => 'required|mimes:jpg,png,jpeg'
        ]);

        $newcover = time().'.'.$request->cover->extension();
        $request->cover->move(public_path('image'), $newcover);


        $buku = new Buku;

        $buku->nama = $request['nama'];
        $buku->penulis = $request['penulis'];
        $buku->penerbit = $request['penerbit'];
        $buku->sinopsis = $request['sinopsis'];
        $buku->kategori_id = $request['kategori_id'];
        $buku->genre_id = $request['genre_id'];
        $buku->cover = $newcover;
        
        $buku->save();

        return redirect('/buku')->withSuccess('Buku Berhasil Ditambahkan');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::find($id);
        return view('buku.detail', ['buku' => $buku]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::find($id);
        $kategori = Kategori::all();
        $genre = Genre::all();
        return view('buku.edit', ['kategori' => $kategori, 'genre' => $genre, 'buku' => $buku]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|max:255',
            'penulis' => 'required',
            'penerbit' => 'required',
            'sinopsis' => 'required',
            'kategori_id' => 'required',
            'genre_id' => 'required',
            'cover' => 'mimes:jpg,png,jpeg'
        ]);

        $buku = Buku::find($id);
        if($request->has('cover')){
            $path = 'image/';
            File::delete($path. $buku->cover);

            $newcover = time().'.'.$request->cover->extension();
            $request->cover->move(public_path('image'), $newcover);

            $buku->cover = $newcover;

            $buku->save();
        }


        $buku->nama = $request['nama'];
        $buku->penulis = $request['penulis'];
        $buku->penerbit = $request['penerbit'];
        $buku->sinopsis = $request['sinopsis'];
        $buku->kategori_id = $request['kategori_id'];
        $buku->genre_id = $request['genre_id'];
        $buku->save();
        
        return redirect('/buku')->withSuccess('Buku Berhasil Dupdate');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::find($id);

        $path = 'image/';
        File::delete($path. $buku->cover);

        $buku -> delete();

        return redirect('/buku')->withSuccess('Buku Berhasil Dihapus');
    }
}
