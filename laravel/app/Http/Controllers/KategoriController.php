<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function __construct()
     {
 
         $this->middleware('auth')->except('tampilKategori', 'show');
 
     } 


    public function tampilKategori(){
        $kategori = DB::table('kategori')->get();
        return view('kategori.index', ['kategori' => $kategori] );
    }

    public function create(){
        return view('kategori.create');
    }

    public function store(request $request){

        // validasi data
        $request->validate([
            'nama' => 'required'
        ]);

        // memasukan data request ke table kategori di database
        DB::table('kategori')->insert([
            'nama' => $request['nama']
        ]);
        
        // arahkan ke halaman /kategori
        return redirect ('/kategori')->withSuccess('Kategori Baru Berhasil Ditambahkan');
    }

    public function show($id){
        $kategori = DB::table('kategori')->find($id);
        $buku = DB::table('buku')->where('kategori_id', $id)->get();

        return view('kategori.detail', ['kategori' => $kategori, 'buku' => $buku]);
    }

    public function edit($id){
        $kategori = DB::table('kategori')->find($id);

        return view('kategori.edit', ['kategori' => $kategori]);
    }

    public function update($id, request $request){
        // validasi data
        $request->validate([
            'nama' => 'required'
        ]);

        DB::table('kategori')
            ->where('id',$id)
            ->update([
                'nama' => $request['nama']
                ]
            );
        return redirect('/kategori')->withSuccess('Kategori Berhasil Diupdate');
    }

    public function destroy($id){
        DB::table('kategori')->where('id',$id)->delete();

        return redirect('/kategori')->withSuccess('Kategori Berhasil Dihapus');
    }
}
